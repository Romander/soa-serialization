using System;
using System.Collections.Concurrent;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace RiatSerializeLab1
{
    public class XmlSerialize : ISerializer
    {
        private static readonly XmlWriterSettings XmlWriterSettings;
        private static readonly XmlSerializerNamespaces XmlNamespaces;
        private static readonly ConcurrentDictionary<Type, XmlSerializer> XmlDictionary;

        static XmlSerialize()
        {
            XmlWriterSettings = new XmlWriterSettings
            {
                OmitXmlDeclaration = true,
                Encoding = new UTF8Encoding(false),
                Indent = false
            };

            XmlNamespaces = new XmlSerializerNamespaces();
            XmlNamespaces.Add("", "");

            XmlDictionary = new ConcurrentDictionary<Type, XmlSerializer>();
        }

        public string Serialize<T>(T obj)
        {
            var xml = XmlDictionary.GetOrAdd(typeof(T), type => new XmlSerializer(type));
            using (var memoryStream = new MemoryStream())
            {
                xml.Serialize(XmlWriter.Create(memoryStream, XmlWriterSettings), obj, XmlNamespaces);
                var bytes = memoryStream.ToArray();

                return Encoding.UTF8.GetString(bytes);
            }
        }

        public T Deserialize<T>(string bytes)
        {
            var xml = XmlDictionary.GetOrAdd(typeof(T), type => new XmlSerializer(type));
            var byteArray = Encoding.UTF8.GetBytes(bytes);
            using (var memoryStream = new MemoryStream(byteArray))
            {
                return (T)xml.Deserialize(memoryStream);
            }
        }
    }
}