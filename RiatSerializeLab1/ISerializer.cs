﻿namespace RiatSerializeLab1
{
    public interface ISerializer
    {
        string Serialize<T>(T obj);
        T Deserialize<T>(string bytes);
    }
}