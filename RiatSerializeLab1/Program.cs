﻿using System;
using System.Linq;

namespace RiatSerializeLab1
{
    public class Program
    {
        static void Main()
        {
            var readLine = Console.ReadLine();
            if (readLine != null && string.Compare(readLine, "Xml", StringComparison.Ordinal) == 0)
            {
                XmlResult();
            }
            else if (readLine != null && string.Compare(readLine, "Json", StringComparison.Ordinal) == 0)
            {
                JsonResult();
            }
        }

        private static void XmlResult()
        {
            var xmlSerializer = new XmlSerialize();
            var input = xmlSerializer.Deserialize<Input>(Console.ReadLine());        
            Console.WriteLine(xmlSerializer.Serialize(CreateOutput(input)));
        }

        private static void JsonResult()
        {
            var jsonSerializer = new JsonSerializer();
            var input = jsonSerializer.Deserialize<Input>(Console.ReadLine());
            Console.WriteLine(jsonSerializer.Serialize(CreateOutput(input)));
        }

        private static Output CreateOutput(Input input)
        {
            var output = new Output();
            output.SumResult = input.Sums.Sum() * input.K;
            output.MulResult = 1;
            foreach (var e in input.Muls)
            {
                output.MulResult *= e;
            }
            output.SortedInputs = ((decimal[])input.Sums.Clone()).Concat(input.Muls.Select(x => (decimal)x)).OrderBy(x => x).ToArray();

            return output;
        }
    }
}
